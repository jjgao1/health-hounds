from fastapi import APIRouter, Depends, Request
from pydantic import BaseModel
from keys import ninja_api_key
import requests
from typing import Dict, Any
from queries.workouts import WorkoutIn, WorkoutOut, Workouts, WorkoutQueries


router = APIRouter()


@router.get('/exercise')
async def get_exercise(
    muscle: str | None = None,
    difficulty: str | None = None,
    type: str | None = None,
    name: str | None = None,
    count: int | None = 2,
):

    url = "https://exercises-by-api-ninjas.p.rapidapi.com/v1/exercises"

    query_string = {"muscle": muscle, "difficulty": difficulty, "type": type, "name": name}

    headers = {
        "X-RapidAPI-Key": ninja_api_key,
        "X-RapidAPI-Host": "exercises-by-api-ninjas.p.rapidapi.com"
    }

    response = requests.get(url, headers=headers, params=query_string)

    return response.json()[:count]


@router.post('/exercise')
async def get_workout(
    request: Request,
    repo: WorkoutQueries = Depends(),
):
    data = await request.json()
    user_id = data.get('user_id')
    exercises = data.get('totalResponseData')
    muscles_worked = data.get('muscles')
    difficulty = data.get('difficulty')
    type = data.get('type')
    workout = WorkoutIn(
        exercises=exercises,
        user_id=user_id,
        muscles_worked=muscles_worked,
        difficulty=difficulty,
        type=type
    )
    return repo.create_workout(workout)
