from fastapi.testclient import TestClient
from queries.workouts import WorkoutQueries
from main import app


client = TestClient(app)


class WorkoutQueriesMock:
    def delete_workout(self, somethin):
        return True


def test_delete_workout():
    app.dependency_overrides[WorkoutQueries] = WorkoutQueriesMock

    response = client.delete('/api/workouts/1')

    assert response.status_code == 200
    assert response.json() == True
