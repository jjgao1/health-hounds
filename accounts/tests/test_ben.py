from queries.thirtyday import (
    ThirtyDayIn,
    ThirtyDayOut,
    ThirtyDayWorkoutQuery,
    ThirtyDayWorkouts,
)
from main import app
from routers.thirtyday import get_all_thirtyday_workouts
from fastapi.testclient import TestClient

client = TestClient(app)


class ThirtyDayQueriesMock:
    def get_all_thirtyday(self):
        return [
            {
                "id": 1,
                "day_id": 1,
                "workout_name": "On Mondays we do Chest",
                "exercise_by_name": "Pullup",
                "instructions": "Pull yourself up",
            },
            {
                "id": 2,
                "day_id": 1,
                "workout_name": "On Mondays we do Chest",
                "exercise_by_name": "Pullup",
                "instructions": "Pull yourself up",
            },
        ]


def test_get_thirtyday():
    app.dependency_overrides[ThirtyDayWorkoutQuery] = ThirtyDayQueriesMock
    response = client.get("/api/thirtyday")

    assert response.status_code == 200
    assert len(response.json()) >= 1

    app.dependency_overrides = {}
