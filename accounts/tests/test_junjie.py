from fastapi.testclient import TestClient
from queries.accounts import AccountsQueries
from main import app


client = TestClient(app)


class AccountsQueriesMock:
    def get_all_accounts(self):
        return [
            {
                "id": 1,
                "username": "jj",
                "email": "jj@email.com",
                "hashed_password": "password",
            },
            {
                "id": 2,
                "username": "junjie",
                "email": "junjie@email.com",
                "hashed_password": "password",
            },
        ]


def test_get_all_accounts():
    app.dependency_overrides[AccountsQueries] = AccountsQueriesMock

    response = client.get("/api/accounts")

    assert response.status_code == 200
    assert len(response.json()) >= 1

app.dependency_overrides = {}
