from fastapi.testclient import TestClient
from main import app
from queries.workouts import WorkoutQueries, WorkoutOut

client = TestClient(app)


class FakeWorkoutQueries(WorkoutQueries):
    def get_workout_by_id(self, id: int) -> WorkoutOut:
        
        return WorkoutOut(
            id=id,
            user_id=1,
            workout_name="Sample Workout",
            muscles_worked=["Legs", "Chest"],
            difficulty="Intermediate",
            exercises=[{"name": "Squats"}, {"name": "Push-ups"}],
        )


def test_get_workout():
    app.dependency_overrides[WorkoutQueries] = FakeWorkoutQueries

    response = client.get("/api/workouts/1")

    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "user_id": 1,
        "workout_name": "Sample Workout",
        "muscles_worked": ["Legs", "Chest"],
        "difficulty": "Intermediate",
        "exercises": [{"name": "Squats"}, {"name": "Push-ups"}],
    }
