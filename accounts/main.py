from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os
from routers import accounts, workouts, exercises, thirtyday
from authenticator import authenticator


app = FastAPI()

origins = [
    "http://localhost:3000",
    os.environ.get("CORS_HOST", None),
    os.environ.get("PUBLIC_URL", None),
    os.environ.get("REACT_APP_ACCOUNTS_HOST", None),
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


connections = []


# router shtuff
app.include_router(thirtyday.router, tags=["thirtyday"])
app.include_router(workouts.router, tags=["workouts"])
app.include_router(authenticator.router)
app.include_router(accounts.router, tags=["accounts"])
app.include_router(exercises.router, tags=["exercises"])

# app.include_router(___.router, tags=["___"])
# app.include_router(___.router, tags=["___"])


@app.get("/")
def read_root():
    return {"Hello": "World"}
