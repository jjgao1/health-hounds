import "./App.css";
import { useGetTokenQuery } from "./redux/api";
import React, { useState } from "react";
import Card from 'react-bootstrap/Card';
import "./ProfilePageCSS.css";
import axios from "axios";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import ListGroup from 'react-bootstrap/ListGroup';
import Container from "react-bootstrap/Container";
import { ToggleButton } from "react-bootstrap";


const ProfilePage = () => {
  const { data: token } = useGetTokenQuery();
  const user = token.account.id;

  const [activeButton, setActiveButton] = useState(false);
  const [gymWorkouts, setGymWorkouts] = useState([]);
  const [homeWorkouts, setHomeWorkouts] = useState([]);

  const handleButtonClick = async (buttonId) => {
    setActiveButton(buttonId)

    const response = await axios.get(`http://localhost:8000/api/workouts/user/${user}`)
    const workoutsGym = response.data.workouts.filter((workout) => workout.type === 'gym');
    const workoutsHome = response.data.workouts.filter((workout) => workout.type === 'home');
    setGymWorkouts(workoutsGym)
    setHomeWorkouts(workoutsHome)
  }

  return (
    <>
    <Container fluid className="d-flex justify-content-center vh-100" style={{ marginTop: "5rem" }}>
      <div className="responsive-div">
      <Row className="justify-content-center align-items-center">
      <Card className="shadow=md custom-rounded">
        <Row className="justify-content-center align-items-center text-center" style={{ marginTop: "1rem" }}>
          <Col>
          <ToggleButton autoFocus onClick={() => handleButtonClick(1)}
            variant={activeButton === 1 ? "primary" : "outline-primary"}
          >Home Workouts</ToggleButton>
          </Col>
          <Col>
          <ToggleButton onClick={() => handleButtonClick(2)}
            variant={activeButton === 2 ? "primary" : "outline-primary"}
          >Gym Workouts</ToggleButton>
          </Col>
        </Row>
        <Card.Body>
          {activeButton === 1 &&
          <>
            <Card.Title></Card.Title>
              {homeWorkouts.length > 0
                ? <>
                <ListGroup>
                  {homeWorkouts.map((workout) => (
                    <ListGroup.Item key={workout.id} action href={`/home-display?workout=${workout.id}`}>
                      {workout.workout_name}
                    </ListGroup.Item>
                  ))}
                </ListGroup>
                </>
                : <p className="text-center"><br></br>No workouts to see here...<br></br>Time to make a new one!</p>
              }
          </>
          }
          {activeButton === 2 &&
          <>
            <Card.Title></Card.Title>
              {gymWorkouts.length > 0
                ? <>
                <ListGroup>
                  {gymWorkouts.map((workout) => (
                    <ListGroup.Item action href={`/gym-display?workout=${workout.id}`}>
                      {workout.workout_name}
                    </ListGroup.Item>
                  ))}
                </ListGroup>
                </>
                : <p className="text-center"><br></br>No workouts to see here...<br></br>Time to make a new one!</p>
              }
          </>
          }
        </Card.Body>
        {!activeButton &&
        <>
          <Card.Text className="text-center">
            Click on a button to display your workouts!
          </Card.Text>
        </>
        }
        <Card.Text className="text-center text-muted" style={{ margin: "2rem" }}>
          Make a New Workout:
          <Card.Link href="/home-workout">For Home</Card.Link>
          <Card.Link href="/gym-workout">For the Gym</Card.Link>
        </Card.Text>
      </Card>
    </Row>
    </div>
    </Container>
    </>
  )
};

export default ProfilePage;
