import { NavLink, Outlet } from 'react-router-dom'
import LogInModal from '../LogInModal'
import { showModalLogIn, LOG_IN_MODAL } from '../redux/accountSlice'
import { useDispatch, useSelector } from 'react-redux';
import { useGetTokenQuery } from '../redux/api';

const ProtectedRoute = () => {
    // const { userInfo } = useSelector((state) => state.account)
    const { data: token, isLoading: tokenLoading } = useGetTokenQuery();
    const dispatch = useDispatch();

  // show unauthorized screen if no user is found in redux store
    if (!token) {
        return (
        <div className='unauthorized'>
            <h1>Unauthorized :(</h1>
            <span>
            <NavLink onClick={() => dispatch(showModalLogIn(LOG_IN_MODAL))}>Login</NavLink> to gain access
            </span>
        </div>
        )
    }

    return <Outlet />
}

export default ProtectedRoute
