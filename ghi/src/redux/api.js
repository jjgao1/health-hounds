import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { clearForm } from './accountSlice';


export const apiSlice = createApi({
  reducerPath: "api",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_ACCOUNTS_HOST,
    prepareHeaders: (headers, { getState }) => {
        const selector = apiSlice.endpoints.getToken.select();
        const { data: tokenData } = selector(getState());
        if (tokenData && tokenData.access_token) {
            headers.set('Authorization', `Bearer ${tokenData.access_token}`);
        }
        return headers;
    }
  }),
  tagTypes: ['Account', 'Workouts', 'Token'],
  endpoints: builder => ({
    // getUserDetails: builder.query({
    //     query: () => ({
    //         url: '/api/token',
    //         method: 'get',
    //     })
    // }),
    signUp: builder.mutation({
        query: data => ({
            url: '/api/accounts',
            method: 'post',
            body: data,
            credentials: 'include',
        }),
        providesTags: ['Account'],
        invalidatesTags: result => {
            return (result && ['Token']) || [];
        },
        async onQueryStarted(arg, { dispatch, queryFulfilled }) {
            try {
                await queryFulfilled;
                dispatch(clearForm());
            } catch (err) {}
        },

    }),
    logIn: builder.mutation({
        query: info => {
            let formData = null;
            if (info instanceof HTMLElement) {
                formData = new FormData(info);
            } else {
                formData = new FormData();
                formData.append('username', info.username);
                formData.append('password', info.password);
            }
            return {
                url: '/token',
                method: 'post',
                body: formData,
                credentials: 'include',
            };
        },
        providesTags: ['Account'],
        invalidatesTags: result => {
            return (result && ['Token']) || [];
        },
        async onQueryStarted(arg, { dispatch, queryFulfilled }) {
            try {
                await queryFulfilled;
                // const { username } = data;
                // const token = data.access_token;
                // localStorage.setItem('userToken', token);
                // dispatch(setUser({ username, userToken: token }));
                dispatch(clearForm());
                // console.log(username);
            } catch (err) {}
        },
    }),
    logOut: builder.mutation({
        query: () => ({
            url: '/token',
            method: 'delete',
            credentials: 'include',
        }),
        invalidateTags: ['Account', 'Token'],
        onQueryFulfilled: () => {
            window.location.href = '/';
        },
    }),
    getToken: builder.query({
        query: () => ({
            url: '/api/token',
            credentials: 'include',
        }),
        providesTags: ['Token'],
    }),
    // getExercise: builder.query({
    //     query: ({ muscle, difficulty, type, name, count }) => ({
    //         url: '/exercise',
    //         body:
    //     }),
    //     query: ({ muscle, difficulty, type, name, count }) => ({
    //         url: '/exercise',
    //         body:
    //     }),
    //     providesTags: data => {
    //         const tags = [{type: ''}]
    //     }
    // })
    // addWorkout: builder.mutation({
    //     query: form => {
    //         const formData = new FormData(form);
    //         const entries = Array.from(forData.entries());
    //         const data = entries.reduce((acc, [key, value]) => {acc[key] = Number.parseInt(value) || value; return acc;}, {});
    //         return {
    //             method: 'post',
    //             url: '/api/workout',
    //             credentials: 'include',
    //             body: data,
    //         }
    //     },
    //     invalidatesTags: [{type: 'Workout', id: 'LIST'}]
    // }),
    // getWorkout: builder.query({
    //     query: () => `/api/workout`,
    //     providesTags: data => {
    //         const tags = [{type: 'Workout', id: 'LIST'}];
    //         if (!data || !data.workout) return tags;

    //         const { workout } = data;
    //         if (books) {
    //             tags.concat(...workout.map(({ id }) => ({type: 'Workout', id})));
    //         }
    //         return tags;
    //     }
    // }),

  }),

});

export const {
    useAddWorkoutMutation,
    useGetWorkoutQuery,
    useGetTokenQuery,
    useLogOutMutation,
    useSignUpMutation,
    useLogInMutation,
    useGetUserDetailsQuery,
} = apiSlice;
