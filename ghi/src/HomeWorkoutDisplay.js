import React, { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { Container } from "react-bootstrap";
import axios from "axios";
import { useLocation } from "react-router-dom";
import NameChangeModal from "./WorkoutNameModal";
import { useNavigate } from "react-router-dom";

const HomeWorkoutDisplay = () => {
  const [exercises, setExercises] = useState([]);
  const [workoutData, setWorkoutData] = useState();
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const workout = queryParams.get("workout")
  const navigate = useNavigate();

  const handleDelete = (event) => {
    event.preventDefault();

    axios.delete(`http://localhost:8000/api/workouts/${workout}`)
      .then((response) => {
        console.log('Deletion successful', response);
      })
      .catch((error) => {
        console.error('Error during deletion', error);
      });
      if (!workoutData?.workout_name) {
        navigate("/home-workout");
      } else {
        navigate("/")
      }

  };

  useEffect(() => {
    const fetchExercises = async () => {
      try {
        const response = await axios.get(`http://localhost:8000/api/workouts/${workout}`, {
          params: {
            workout,
          },
        })

        const allExercises = response.data.exercises[0];
        const workoutGrabbed =response.data;
        setWorkoutData(workoutGrabbed)
        setExercises(allExercises)
      } catch (error) {
        console.error(error);
      }
    };

    fetchExercises();
  }, [workout]);

  const workoutName = workoutData?.workout_name;

  return (
    <>
      <div padding="20px">
        {!workoutName || workoutName === ''
          ? <h1>Your New Home Workout: </h1>
          : <h1>{workoutData.workout_name}</h1>
        }
        <ul>
          {exercises.map((exercise) => (
            <li key={exercise.name}>
              <h3>{exercise.name}</h3>
              <p>Instructions: {exercise.instructions}</p>
              <p>Type: {exercise.type}</p>
              <p>Difficulty: {exercise.difficulty}</p>
            </li>
          ))}
        </ul>
      </div>
      <Container padding="20px">
      <div>
        {!workoutName || workoutName === '' ? (
          <>
            <NameChangeModal workout={workout} />{' '}
            <Button onClick={handleDelete}>
              Don't Save
            </Button>
          </>
        ) : (
          <>
            <Button onClick={handleDelete}>
              Delete
            </Button>
          </>
        )}
      </div>
    </Container>
    </>
  );
};

export default HomeWorkoutDisplay;

// This is the page that will display the exercises of the types selected
