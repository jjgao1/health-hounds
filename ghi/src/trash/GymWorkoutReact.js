import "./App.css";
import { useGetTokenQuery } from "./redux/api";
import React, { useState } from "react";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import Checkbox from "react-bootstrap/Checkbox";
import axios from "axios";

const GymWorkout = () => {
  const [muscles] = useState(["glutes", "chest", "forearms", "triceps"]);
  const [difficulties] = useState(["beginner", "intermediate", "expert"]);
  const [selectedMuscles, setSelectedMuscles] = useState([]);
  const [selectedDifficulty, setSelectedDifficulty] = useState("");
  const [setResponseData] = useState(null);
  const { data: token, isLoading: tokenLoading } = useGetTokenQuery();

  const handleChangeMuscle = (event) => {
    const { name, checked } = event.target;
    console.log(`Muscle: ${name}, Checked: ${checked}`);
    if (checked) {
      setSelectedMuscles((prevSelectedMuscles) => [
        ...prevSelectedMuscles,
        name,
      ]);
    } else {
      setSelectedMuscles((prevSelectedMuscles) =>
        prevSelectedMuscles.filter((muscle) => muscle !== name)
      );
    }
  };

  const handleChangeDifficulty = (event) => {
    const value = event.target.value;
    console.log(`Difficulty: ${value}`);
    setSelectedDifficulty(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    console.log("selected muscles ",selectedMuscles);

    let totalResponseData = [];

    for (let muscle of selectedMuscles) {
      try {
        const response = await axios.get("http://localhost:8000/exercise", {
          params: {
            muscle: muscle,
            difficulty: selectedDifficulty,

          },
        });

        totalResponseData = totalResponseData.concat(response.data)

        console.log("single exercise ",response.data);
      } catch (error) {
        console.error(error);
      }
  }

  console.log("total response ",totalResponseData);

  const allData = {
    'totalResponseData': totalResponseData,
    'user_id': token.account.id,
    'muscles': selectedMuscles,
    'difficulty': selectedDifficulty,
  }

  // Sending the response data to the backend
  try {
    const backendResponse = await axios.post(
      "http://localhost:8000/exercise",
      allData,
    );
    console.log(backendResponse.data);
  } catch (error) {
    console.error(error);
  }

  };

  return (
    <Container>
      <h1 className="text-center mt-3 mb-2">Gym Workout Generator</h1>
      <Row className="justify-content-center">
        <Col xs={6}>
          <Form onSubmit={handleSubmit}>
            <Form.Group>
              {muscles.map((muscle) => (
                <Checkbox
                  key={muscle}
                  type="checkbox"
                  id={muscle}
                  // label={muscle}
                  checked={selectedMuscles.includes(muscle)}
                  onChange={handleChangeMuscle}
                  className="mb-3"
                >
                  <Form.Check.Input type="checkbox" name={muscle} />
                  <Form.Label> { muscle}</Form.Label>
                </Checkbox>
                // />
              ))}
            </Form.Group>
            <Form.Group>
              <Form.Label>Difficulty:</Form.Label>
              {difficulties.map((difficulty) => (
                <Form.Check
                  key={difficulty}
                  type="radio"
                  id={difficulty}
                  label={difficulty}
                  name="difficulty"
                  value={difficulty}
                  checked={selectedDifficulty === difficulty}
                  onChange={handleChangeDifficulty}
                  className="mb-3"
                />
              ))}
            </Form.Group>
            {/* <Form.Group>
              <Form.Label>Number of Exercises:</Form.Label>
              <Form.Control
                type="text"
                value={count}
                onChange={handleChange}
              />
            </Form.Group> */}
            <Button type="submit" className="mt-3">Submit</Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

//   return (
//     <div>
//       <h1
//         style={{ paddingTop: "3px", textAlign: "center", paddingBottom: "2px" }}
//       >
//         Gym Workout Generator
//       </h1>
//       <div style={{ display: "flex", justifyContent: "center" }}>
//         <form onSubmit={handleSubmit}>
//           <div>
//             {muscles.map((muscle) => (
//               <div key={muscle} style={{ marginBottom: "10px" }}>
//                 <label htmlFor={muscle}>
//                   <input
//                     type="checkbox"
//                     id={muscle}
//                     name={muscle}
//                     checked={selectedMuscles.includes(muscle)}
//                     onChange={handleChangeMuscle}
//                     style={{ marginRight: "5px" }}
//                   />
//                   {muscle}
//                 </label>
//               </div>
//             ))}
//           </div>
//           <div>
//             <p>Difficulty:</p>
//             {difficulties.map((difficulty) => (
//               <div key={difficulty} style={{ marginBottom: "10px" }}>
//                 <label htmlFor={difficulty}>
//                   <input
//                     type="radio"
//                     id={difficulty}
//                     name="difficulty"
//                     value={difficulty}
//                     checked={selectedDifficulty === difficulty}
//                     onChange={handleChangeDifficulty}
//                     style={{ marginRight: "5px" }}
//                   />
//                   {difficulty}
//                 </label>
//               </div>
//             ))}
//           </div>
//           {/* <div> */}
//             {/* <p>Number of Exercises</p>
//             <div>
//               <input type="text" value={count} onChange={handleChange}
//             </div>
//           </div> */}
//           <button type="submit" style={{ marginTop: "10px" }}>
//             Submit
//           </button>
//         </form>
//       </div>
//     </div>
//   );
// };

export default GymWorkout;
