import "./App.css";
import { useGetTokenQuery } from "./redux/api";
import React, { useState } from "react";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Button from "react-bootstrap/Button";
import axios from "axios";
import TestDisplay from "./TestDisplay";
import { useNavigate } from "react-router-dom";

const GymWorkout = () => {
  const [count, setCount] = useState(1);
  const [muscles] = useState([
    "glutes",
    "chest",
    "forearms",
    "triceps",
    "abdominals",
    "abductors",
    "adductors",
    "biceps",
    "calves",
    "hamstrings",
    "lats",
    "lower_back",
    "middle_back",
    "neck",
    "quadriceps",
    "traps",
  ]);
  const [difficulties] = useState(["beginner", "intermediate", "expert"]);
  const [selectedMuscles, setSelectedMuscles] = useState([]);
  const [selectedDifficulty, setSelectedDifficulty] = useState("");
  const [setResponseData] = useState(null);
  const { data: token, isLoading: tokenLoading } = useGetTokenQuery();

  const handleChangeCount = (event) => {
    setCount(event.target.value);
  };

  const navigate = useNavigate();

  const handleChangeMuscle = (event) => {
    const { name, checked } = event.target;
    if (checked) {
      setSelectedMuscles((prevSelectedMuscles) => [
        ...prevSelectedMuscles,
        name,
      ]);
    } else {
      setSelectedMuscles((prevSelectedMuscles) =>
        prevSelectedMuscles.filter((muscle) => muscle !== name)
      );
    }
  };

  const handleChangeDifficulty = (event) => {
    setSelectedDifficulty(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const muscleQuery = selectedMuscles.join(",");
    navigate(`/test-display?muscles=${muscleQuery}`);
    console.log("selected muscles ", selectedMuscles);

    const totalMuscles = Math.min(selectedMuscles.length, count); // Limit selected types to the total count
    const exercisesPerMuscle = Math.floor(count / totalMuscles); // Calculate exercises per type
    const remainingExercises = count % totalMuscles;

    let totalResponseData = [];

    // for (let muscle of selectedMuscles) {
    for (let i = 0; i < totalMuscles; i++) {
      const muscle = selectedMuscles[i];
      let muscleCount = exercisesPerMuscle; // Set default count per type

      if (i < remainingExercises) {
        muscleCount += 1; // Add remaining exercise to the current type
    }
      try {
        const response = await axios.get("http://localhost:8000/exercise", {
          params: {
            muscle: muscle,
            difficulty: selectedDifficulty,
            count: muscleCount
          },
        });

        totalResponseData = totalResponseData.concat(response.data);

        console.log("single exercise ", response.data);
      } catch (error) {
        console.error(error);
      }
    }

    console.log("total response ", totalResponseData);

    const allData = {
      totalResponseData: totalResponseData,
      user_id: token.account.id,
      muscles: selectedMuscles,
      difficulty: selectedDifficulty,
    };

    // Sending the response data to the backend
    try {
      const backendResponse = await axios.post(
        "http://localhost:8000/exercise",
        allData
      );
      console.log(backendResponse.data);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div>
      <h1
        style={{ paddingTop: "3px", textAlign: "center", paddingBottom: "2px" }}
      >
        Gym Workout Generator
      </h1>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <form onSubmit={handleSubmit}>
          <div>
            {muscles.map((muscle) => (
              <div key={muscle} style={{ marginBottom: "10px" }}>
                <label htmlFor={muscle}>
                  <input
                    type="checkbox"
                    id={muscle}
                    name={muscle}
                    checked={selectedMuscles.includes(muscle)}
                    onChange={handleChangeMuscle}
                    style={{ marginRight: "5px" }}
                  />
                  {muscle}
                </label>
              </div>
            ))}
          </div>
          <div>
            <p>Difficulty:</p>
            {difficulties.map((difficulty) => (
              <div key={difficulty} style={{ marginBottom: "10px" }}>
                <label htmlFor={difficulty}>
                  <input
                    type="radio"
                    id={difficulty}
                    name="difficulty"
                    value={difficulty}
                    checked={selectedDifficulty === difficulty}
                    onChange={handleChangeDifficulty}
                    style={{ marginRight: "5px" }}
                  />
                  {difficulty}
                </label>
              </div>
            ))}
          </div>
          {/* <div> */}
          {/* <p>Number of Exercises</p>
            <div>
              <input type="text" value={count} onChange={handleChange}
            </div>
          </div> */}
            <p>Number of Exercises:</p>
            <div>
                <input
                    type="text"
                    value={count}
                    onChange={handleChangeCount}
                />
            </div>
          <button type="submit" style={{ marginTop: "10px" }}>
            Submit
          </button>
        </form>
      </div>
    </div>
  );
};

export default GymWorkout;
