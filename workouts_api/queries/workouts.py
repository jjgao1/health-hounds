from pydantic import BaseModel
from typing import Optional, List
from queries.pool import pool


class Error(BaseModel):
    message: str


class WorkoutIn(BaseModel):
    name: str
    account_id: str
    home_workout: bool
    exercises: list
    description: Optional[str]
    rating: Optional[int]
    comments: Optional[str]


class WorkoutOut(WorkoutIn):
    id: int


class Workouts(BaseModel):
    workouts: List[WorkoutOut]


class WorkoutQueries:
    def get_all(self) -> Union[Error, List[WorkoutOut]]:


    def get_by_
