## User Interface

## Landing Page

This is the page that users will land on when they visit the website. This page have a navigation bar that allows users to visit our `About Us` page, as well as buttons to Sign up or Log in.

## Sign up and Log in Modals

When a user clicks the Sign up button, our modal will pop up over the landing page with selectable text fields to enter their username, password, and email. The log in button acts similarly, just without the email field

## Profile Page

This is the page that a user will be redirected to following a successful sign up or log in. It will display the workouts that the logged in user has previously saved. The user can select which saved workout they would like to view.

## About Us Page

This page contains gmail and LinkedIn links for each developer who created Health Hounds

## Gym Workouts Page

This page is where users will go to select, from a list of options, which muscles they would like to target in their workout, and the difficulty of the exercise for said muscles. Additionally, users are able to enter the amount of exercises they would like included in the workout that is generated for them.

## Gym Workouts Display Page

This is the page which users are redirected to on submission of their form on the Gym Workouts page. This page neatly lists the exercises with instructions, the muscle the exercise targets, and its difficulty for each. This is also where users have the choice to save the workout which was created for them, or to not save it. The latter of which would redirect a user back to the Gym Workouts page.

## Home Workouts Page

This page is where users will go to select, from a list of options, which type of workout they want to do, and the difficulty of the exercises. Additionally, users are able to enter the amount of exercises they would like included in the workout that is generated for them.

## Home Workouts Display Page

This is the page which users are redirected to on submission of their form on the Home Workouts page. This page neatly lists the exercises with instructions, the type of exercise, and its difficulty for each. This is also where users have the choice to save the workout which was created for them, or to not save it. The latter of which would redirect a user back to the Home Workouts page.

## Thirty Day Program

This page users can select from the nav bar which has a preset 30-day workout plan. The page instructions on how users should proceed with the exercises as the weeks progress over the course of a month. The user also has the option to play a song while they are on this page.
